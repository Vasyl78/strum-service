# frozen_string_literal: true

RSpec.describe "failure with param" do
  def call_service(service_class, input, error_param)
    service_class.call(input) do |m|
      m.success { |result| result }
      m.failure(error_param) { |errors| errors }
      m.failure(:other_param) { "Other param failure" }
      m.failure { "Default failure" }
    end
  end

  it "basic behavior" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        add_error(:err_key, :err_val)
      end
    end

    expect(call_service(service_class, {}, :err_val)).to eq({ err_key: [:err_val] })
  end

  it "multi params order" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        add_error(:err_key, :err_val)
        add_error(:err_key, :other_param)
      end
    end

    expect(call_service(service_class, {}, :err_val)).to eq({ err_key: %i[err_val other_param] })
  end

  it "multi params incorrect order" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        add_error(:err_key, :other_param)
        add_error(:err_key, :err_val)
      end
    end

    expect(call_service(service_class, {}, :err_val)).to eq("Other param failure")
  end
end
