# frozen_string_literal: true

RSpec.describe "args" do
  let(:args_service_class) do
    Class.new do
      include Strum::Service
      define_method(:call) do
        output(test_arg)
      end
    end
  end

  [
    { name: "arg value is integer", value: 10 },
    { name: "arg value is hash", value: { demo: :demo } },
    { name: "arg value is string", value: "demo value" },
    { name: "arg value is boolean", value: true }
  ].each do |params|
    it params[:name] do
      expect(args_service_class.call({}, test_arg: params[:value])).to eq(params[:value])
    end
  end
end
